from django.contrib.auth import authenticate, login, logout, models
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .models import User, MenuItem, Topping, CartItem, Order

def index(request):
    if not request.user.is_authenticated:
        return render(request, "orders/login.html", {"message": None})
    context = reset_selections(request)
    return render(request, "orders/user.html", context)

def login_view(request):
    username = request.POST["username"]
    password = request.POST["password"]
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return HttpResponseRedirect(reverse("index"))
    else:
        return render(request, "orders/login.html", {"message": "Invalid credentials."})

def register_view(request):
    try:
        username = request.POST["username"]
    except:
        return render(request, "orders/register.html", {"message": None})

    password = request.POST["password"]
    email = request.POST["email"]
    first_name = request.POST["first_name"]
    last_name = request.POST["last_name"]

    try:
        authUser = models.User.objects.create_user(username=username,
            email=email,
            password=password,
            first_name=first_name,
            last_name=last_name)
    except Exception as e:
        if "UNIQUE constraint failed" in str(e):
            return render(request, "orders/register.html", {"message": "Registration Error: Username already in use. Choose another."})
        else:
            return render(request, "orders/register.html", {"message": "Registration Error: " + str(e)})

    phone_number = request.POST["phone_number"]
    d_address1 = request.POST["d_address1"]
    print(d_address1)
    d_address2 = request.POST["d_address2"]
    if d_address2 == "":
        d_address2 = "N/A"
    d_zip_code = request.POST["d_zip_code"]
    d_city = request.POST["d_city"]
    b_address1 = request.POST["b_address1"]
    b_address2 = request.POST["b_address2"]
    b_zip_code = request.POST["b_zip_code"]
    b_city = request.POST["b_city"]
    if b_address1 == "":
        b_address1 = d_address1
    if b_address2 == "":
        b_address2 = d_address2
    if b_zip_code == "":
        b_zip_code = d_zip_code
    if b_city == "":
        b_city = d_city

    newUser = User(user = authUser,
        phone_number = phone_number,
        d_address1 = d_address1,
        d_address2 = d_address2,
        d_zip_code = d_zip_code,
        d_city = d_city,
        b_address1 = b_address1,
        b_address2 = b_address2,
        b_zip_code = b_zip_code,
        b_city = d_city)
    newUser.save()

    if newUser is not None:
        login(request, authUser)
        return HttpResponseRedirect(reverse("index"))
    else:
        return render(request, "orders/register.html", {"message": "Registration Error"})

def change_info(request):
    if request.POST.get("change"):
            return render(request, "orders/change.html", {"message": None, "user": User.objects.get(user=request.user)})
    else:
        try:
            email = request.POST["email"]
            password = request.POST["password"]
            first_name = request.POST["first_name"]
            last_name = request.POST["last_name"]

            models.User.objects.filter(username=request.user.username).update(
                email=email,
                password=password,
                first_name=first_name,
                last_name=last_name)

            phone_number = request.POST["phone_number"]
            d_address1 = request.POST["d_address1"]
            d_address2 = request.POST["d_address2"]
            d_zip_code = request.POST["d_zip_code"]
            d_city = request.POST["d_city"]
            b_address1 = request.POST["b_address1"]
            b_address2 = request.POST["b_address2"]
            b_zip_code = request.POST["b_zip_code"]
            b_city = request.POST["b_city"]

            User.objects.filter(user = request.user).update(
                phone_number = phone_number,
                d_address1 = d_address1,
                d_address2 = d_address2,
                d_zip_code = d_zip_code,
                d_city = d_city,
                b_address1 = b_address1,
                b_address2 = b_address2,
                b_zip_code = b_zip_code,
                b_city = d_city)

            context = {
                "order": Order.objects.get(id=request.session['currentOrder'])
            }
            return render(request, "orders/order.html", context)
        except Exception as e:
            return render(request, "orders/change.html", {"message": "Error: " + str(e), "user": User.objects.get(user=request.user)})

def logout_view(request):
    logout(request)
    return render(request, "orders/login.html", {"message": "Logged out."})

def reset_selections(request):
    selections = set()

    for s in MenuItem.objects.values('selection'):
        selections.add(s['selection'])
    request.session['selection'] = list(selections)

    request.session['subselection'] = None
    request.session['size'] = None
    request.session['toppings'] = None
    request.session['ptoppings'] = []

    context = {
        "user": request.user,
        "selection": request.session['selection'],
        "subselection": None,
        "size": None,
        "toppings": None,
        "ptoppings": [],
        "cart": User.objects.get(user=request.user).cart.all
    }
    return context

def select_selection(request, selection):
    request.session['selection'] = selection
    subselections = set()
    for s in MenuItem.objects.filter(selection=selection).values('name'):
        subselections.add(s['name'])
    request.session['subselection'] = list(subselections)
    context = {
           "user": request.user,
           "selection": [selection],
           "subselection": subselections,
           "size": None,
           "toppings": None,
           "ptoppings": [],
           "cart": User.objects.get(user=request.user).cart.all
    }

    return render(request, "orders/user.html", context)

def select_subselection(request, subselection):
    request.session['subselection'] = subselection
    sizes = set()
    for s in MenuItem.objects.filter(selection=request.session['selection']).filter(name=subselection).values('size'):
        sizes.add(s['size'])
    request.session['size'] = list(sizes)
    context = {
           "user": request.user,
           "selection": [request.session['selection']],
           "subselection": [request.session['subselection']],
           "size": sizes,
           "toppings": None,
           "ptoppings": [],
           "cart": User.objects.get(user=request.user).cart.all
    }

    return render(request, "orders/user.html", context)

def select_size(request, size):
    request.session['size'] = size

    if request.session['selection'] == "Pizza":
        toppings = set()
        for t in Topping.objects.values('name'):
            toppings.add(t['name'])
        request.session['toppings'] = list(toppings)
        context = {
           "user": request.user,
           "selection": [request.session['selection']],
           "subselection": [request.session['subselection']],
           "size": [size],
           "toppings": toppings,
           "ptoppings": [],
           "cart": User.objects.get(user=request.user).cart.all
        }
    elif request.session['selection'] == "Sub" and request.session['subselection'] == "Steak+Cheese":
        toppings = ["Mushrooms", "GreenPeppers", "Onions"]
        request.session['toppings'] = toppings
        context = {
           "user": request.user,
           "selection": [request.session['selection']],
           "subselection": [request.session['subselection']],
           "size": [size],
           "toppings": toppings,
           "ptoppings": [],
           "cart": User.objects.get(user=request.user).cart.all
        }
    else:
        request.session['toppings'] = None
        context = {
           "user": request.user,
           "selection": [request.session['selection']],
           "subselection": [request.session['subselection']],
           "size": [size],
           "ptoppings": [],
           "toppings": None,
           "cart": User.objects.get(user=request.user).cart.all
        }

    return render(request, "orders/user.html", context)

def select_toppings(request, ptoppings):
    curtoppings = request.session['ptoppings']
    curtoppings.append(ptoppings)
    request.session['ptoppings'] = curtoppings
    context = {
           "user": request.user,
           "selection": [request.session['selection']],
           "subselection": [request.session['subselection']],
           "size": [request.session['size']],
           "toppings": request.session['toppings'],
           "ptoppings": request.session['ptoppings'],
           "cart": User.objects.get(user=request.user).cart.all
    }

    return render(request, "orders/user.html", context)

def reset_button(request):
    context = reset_selections(request)
    return render(request, "orders/user.html", context)

def add_to_cart(request):
    if request.session['selection'] == None \
        or request.session['subselection'] == None \
        or request.session['size'] == None \
        or isinstance(request.session['size'], list):
        context = reset_selections(request)
        context['message1'] = "Please completely fill your selections"
        return render(request, "orders/user.html", context)

    ptoppings = request.session['ptoppings']
    if ptoppings == None or ptoppings == []:
        numtop = 0
    else:
        numtop = len(request.session['ptoppings'])

    if request.session['selection'] == "Pizza" and numtop > 4:
        menuItem = MenuItem.objects.get(selection = request.session['selection'], \
            name = request.session['subselection'], \
            size = request.session['size'], \
            numbertoppings = 4 )
    else:
        menuItem = MenuItem.objects.get(selection = request.session['selection'], \
            name = request.session['subselection'], \
            size = request.session['size'], \
            numbertoppings = numtop )

    currentBuyerID = User.objects.get(user=request.user)
    newCartItem = CartItem(item = menuItem)
    newCartItem.save()

    if numtop > 0:
        for topping in ptoppings:
            currentToppingID = Topping.objects.get(name=topping)
            newCartItem.toppings.add(currentToppingID)

    currentBuyerID.cart.add(newCartItem)

    context = reset_selections(request)

    return render(request, "orders/user.html", context)

def remove_from_cart(request):
    checked = request.POST.getlist('checks[]')

    cart = User.objects.get(user=request.user).cart.all

    objects = []
    for c in checked:
        objects.append(cart()[int(c)])

    for o in objects:
        User.objects.get(user=request.user).cart.remove(o)
        o.delete()

    context = reset_selections(request)
    return render(request, "orders/user.html", context)

def place_order(request):
    checked = request.POST.getlist('checks[]')
    cart = User.objects.get(user=request.user).cart.all

    objects = []
    for c in checked:
        objects.append(cart()[int(c)])

    if len(objects) > 0:
        currentBuyerID = User.objects.get(user=request.user)
        newOrder = Order(orderer = currentBuyerID, totalprice = 0, status = 'started')
        newOrder.save()

        total = 0
        for o in objects:
            total += o.item.price
            #User.objects.get(user=request.user).cart.remove(o) remove in confirm order
            currentItemID = CartItem.objects.get(id=o.id)
            newOrder.items.add(currentItemID)

        newOrder.totalprice = total
        newOrder.save()
        request.session['currentOrder'] = newOrder.id

        context = {
            "order": newOrder
        }
        return render(request, "orders/order.html", context)
    else:
        context = reset_selections(request)
        context['message2'] = "Please check on cart item to place order"
        return render(request, "orders/user.html", context)

def confirm_order(request):
    order = Order.objects.get(id=request.session['currentOrder'])

    orderTime = (str(order.created_on).split()[1]).split(":")
    hour = int(orderTime[0])
    minute = int(orderTime[1])
    checktime = (hour - 4) % 23
    if checktime < 10 or checktime > 23:
        context = reset_selections(request)
        context['message2'] = "Please return during business hours 10 am to 11 pm to place order"
        return render(request, "orders/user.html", context)

    btimemin = (minute + 15) % 59
    btimerem = int((minute + 15) / 59)
    btimehour = hour + btimerem

    etimemin = (minute + 30) % 59
    etimerem = int((minute + 30) / 59)
    etimehour = hour + etimerem

    # GMT to EST
    btimehour = (btimehour - 4) % 23
    etimehour = (etimehour - 4) % 23

    #convert from military
    if btimehour > 12:
        btimehour -= 12
        btime = "{:02d}".format(btimehour) + ":" + "{:02d}".format(btimemin) + "pm"
    else:
        btime = "{:02d}".format(btimehour) + ":" + "{:02d}".format(btimemin) + "am"
    if etimehour > 12:
        etimehour -= 12
        etime = "{:02d}".format(etimehour) + ":" + "{:02d}".format(etimemin) + "pm"
    else:
        etime = "{:02d}".format(etimehour) + ":" + "{:02d}".format(etimemin) + "am"

    items = order.items.all()
    for i in items:
        User.objects.get(user=request.user).cart.remove(i)

    order.status = "confirmed"
    order.save()

    context = {
        "order": order,
        "btime": btime,
        "etime": etime
    }
    return render(request, "orders/order.html", context)

def cancel_order(request):
    # go back to cart without removing items from cart
    context = reset_selections(request)
    return render(request, "orders/user.html", context)
