from django.contrib import admin

from .models import User, MenuItem, Topping, CartItem, Order

#cart item many to many to toppings
class CartItemInline(admin.StackedInline):
    model = CartItem.toppings.through
    extra = 1

class ToppingAdmin(admin.ModelAdmin):
    inlines = [CartItemInline]

class CartItemAdmin(admin.ModelAdmin):
    filter_horizontal = ("toppings",)

# Order many to many to cart items
class OrderInline(admin.StackedInline):
    model = Order.items.through
    extra = 1

class OrderItemsAdmin(admin.ModelAdmin):
    inlines = [OrderInline]

    actions = ['delete_model', 'delete_queryset']

    def delete_model(self, request, obj):
        for o in obj:
            for i in o.items.all():
                o.items.remove(i)
                i.delete()
            o.delete()

    def delete_queryset(self, request, obj):
        for o in obj:
            for i in o.items.all():
                o.items.remove(i)
                i.delete()
            o.delete()


class OrderAdmin(admin.ModelAdmin):
    filter_horizontal = ("items",)


# User many to many to cart items
class UserInline(admin.StackedInline):
    model = User.cart.through
    extra = 1

class UserAdmin(admin.ModelAdmin):
    inlines = [UserInline]

class UserAdmin(admin.ModelAdmin):
    filter_horizontal = ("cart",)


admin.site.register(MenuItem)
admin.site.register(Topping, ToppingAdmin)
admin.site.register(CartItem, CartItemAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Order, OrderItemsAdmin)
