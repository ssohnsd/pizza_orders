from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import RegexValidator

class MenuItem(models.Model):
    selection = models.CharField(max_length=64)
    name = models.CharField(max_length=128)
    size = models.CharField(max_length=64)
    numbertoppings = models.IntegerField()
    price = models.FloatField()

    def __str__(self):
        return f"{self.selection} {self.name} {self.size} \
            {self.numbertoppings} {self.price}"

class Topping(models.Model):
    name = models.CharField(max_length=64)
    def __str__(self):
        return f"{self.name}"

class CartItem(models.Model):
    item = models.ForeignKey(MenuItem, on_delete=models.CASCADE, related_name="item")
    toppings = models.ManyToManyField(Topping, blank=True, related_name="toppings")

    def __str__(self):
        printout = f"{self.item.selection} {self.item.name} {self.item.size} \
            {self.item.numbertoppings} {self.item.price}"
        try:
            for top in self.toppings.all():
                printout = printout + ' ' + str(top.name)
        except:
            pass
        return printout

class User(models.Model):
    user = models.ForeignKey(
      get_user_model(),
      on_delete=models.CASCADE
    )
    #username, email, password, first_name, and last_name already above
    #TODO: credit card
    cart = models.ManyToManyField(CartItem, blank=True, related_name="cart")
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    d_address1 = models.CharField("Address line 1", max_length=1024)
    d_address2 = models.CharField("Address line 2", max_length=1024, blank=True, null=True)
    d_zip_code = models.CharField("ZIP", max_length=12)
    d_city = models.CharField("City", max_length=1024)
    b_address1 = models.CharField("Address line 1", max_length=1024)
    b_address2 = models.CharField("Address line 2", max_length=1024, blank=True, null=True)
    b_zip_code = models.CharField("ZIP", max_length=12)
    b_city = models.CharField("City", max_length=1024)

    def __str__(self):
        return f"{self.user.username} {self.phone_number} {self.d_city}"

class Order(models.Model):
    orderer = models.ForeignKey(User, on_delete=models.CASCADE, related_name="orderer")
    items = models.ManyToManyField(CartItem, blank=True, related_name="orderitems")
    totalprice = models.FloatField()
    created_on = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=64)

    def __str__(self):
        printout = f"{self.orderer.user.username}: {self.created_on} {self.status}"
        try:
            for item in self.items.all():
                printout = printout + ' ' + str(item)
        except:
            pass
        return printout
