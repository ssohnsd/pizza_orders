from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("login", views.login_view, name="login"),
    path("register", views.register_view, name="register"),
    path("logout", views.logout_view, name="logout"),
    path("add_to_cart", views.add_to_cart, name="add_to_cart"),
    path("reset_button", views.reset_button, name="reset_button"),
    path("place_order", views.place_order, name="place_order"),
    path("remove_from_cart", views.remove_from_cart, name="remove_from_cart"),
    path("select_selection/<selection>/", views.select_selection, name="select_selection"),
    path("select_subselection/<subselection>/", views.select_subselection, name="select_subselection"),
    path("select_size/<size>/", views.select_size, name="select_size"),
    path("select_toppings/<ptoppings>/", views.select_toppings, name="select_toppings"),
    path("confirm_order", views.confirm_order, name="confirm_order"),
    path("cancel_order", views.cancel_order, name="cancel_order"),
    path("change_info", views.change_info, name="change_info")
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
