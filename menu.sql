DELETE FROM orders_menuitem;

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Regular','Small', 0, 12.20);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Regular','Large', 0, 17.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Regular','Small', 1, 13.20);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Regular','Large', 1, 19.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Regular','Small', 2, 14.70);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Regular','Large', 2, 21.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Regular','Small', 3, 15.70);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Regular','Large', 3, 23.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Regular','Small', 4, 17.25);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Regular','Large', 4, 25.45);

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Sicilian','Small', 0, 23.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Sicilian','Large', 0, 37.70);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Sicilian','Small', 1, 25.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Sicilian','Large', 1, 39.70);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Sicilian','Small', 2, 27.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Sicilian','Large', 2, 41.70);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Sicilian','Small', 3, 28.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Sicilian','Large', 3, 43.70);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Sicilian','Small', 4, 29.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pizza', 'Sicilian','Large', 4, 44.70);

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Cheese', 'Small', 0, 6.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Cheese', 'Large', 0, 7.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Italian', 'Small', 0, 6.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Italian', 'Large', 0, 7.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Ham+Cheese', 'Small', 0, 6.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Ham+Cheese', 'Large', 0, 7.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Meatball', 'Small', 0, 6.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Meatball', 'Large', 0, 7.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Tuna', 'Small', 0, 6.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Tuna', 'Large', 0, 7.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Turkey', 'Small', 0, 7.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Turkey', 'Large', 0, 8.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'ChickenParmigiana', 'Small', 0, 7.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'ChickenParmigiana', 'Large', 0, 8.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'EggplantParmigiana', 'Small', 0, 6.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'EggplantParmigiana', 'Large', 0, 7.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Steak', 'Small', 0, 6.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Steak', 'Large', 0, 7.95);

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Steak+Cheese', 'Small', 0, 6.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Steak+Cheese', 'Large', 0, 8.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Steak+Cheese', 'Small', 1, 7.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Steak+Cheese', 'Large', 1, 9.00);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Steak+Cheese', 'Small', 2, 7.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Steak+Cheese', 'Large', 2, 9.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Steak+Cheese', 'Small', 3, 8.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Steak+Cheese', 'Large', 3, 10.00);

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'SausagePepOnion', 'Small', 0, 6.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'SausagePepOnion', 'Large', 0, 8.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Hamburger', 'Small', 0, 4.60);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Hamburger', 'Large', 0, 6.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'CheeseBurger', 'Small', 0, 5.20);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'CheeseBurger', 'Large', 0, 7.45);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'FriedChicken', 'Small', 0, 6.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'FriedChicken', 'Large', 0, 8.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Veggie', 'Small', 0, 6.95);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Sub', 'Veggie', 'Large', 0, 8.50);

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pasta', 'ZitiMozzarella', 'Regular', 0, 6.50);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pasta', 'ZitiMozzarella', 'SmallPlatter', 0, 35.00);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pasta', 'ZitiMozzarella', 'LargePlatter', 0, 60.00);

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pasta', 'ZitiMeatball', 'Regular', 0, 8.75);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pasta', 'ZitiMeatball', 'SmallPlatter', 0, 45.00);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pasta', 'ZitiMeatball', 'LargePlatter', 0, 70.00);

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pasta', 'ZitiChicken', 'Regular', 0, 9.75);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pasta', 'ZitiChicken', 'SmallPlatter', 0, 45.00);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Pasta', 'ZitiChicken', 'LargePlatter', 0, 80.00);

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Salad', 'Garden', 'Regular', 0, 6.25);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Salad', 'Garden', 'SmallPlatter', 0, 35.00);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Salad', 'Garden', 'LargePlatter', 0, 60.00);

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Salad', 'Greek', 'Regular', 0, 8.25);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Salad', 'Greek', 'SmallPlatter', 0, 45.00);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Salad', 'Greek', 'LargePlatter', 0, 70.00);

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Salad', 'Antipasto', 'Regular', 0, 8.25);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Salad', 'Antipasto', 'SmallPlatter', 0, 45.00);
INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Salad', 'Antipasto', 'LargePlatter', 0, 70.00);

INSERT INTO orders_menuitem ( selection, name, size, numbertoppings, price ) VALUES ( 'Salad', 'Tuna', 'Regular', 0, 8.25);

DELETE FROM orders_topping;

INSERT INTO orders_topping ( name ) VALUES ( 'Pepperoni');
INSERT INTO orders_topping ( name ) VALUES ( 'Sausage');
INSERT INTO orders_topping ( name ) VALUES ( 'Mushrooms');
INSERT INTO orders_topping ( name ) VALUES ( 'Onions');
INSERT INTO orders_topping ( name ) VALUES ( 'Ham');
INSERT INTO orders_topping ( name ) VALUES ( 'CanadianBacon');
INSERT INTO orders_topping ( name ) VALUES ( 'Pineapple');
INSERT INTO orders_topping ( name ) VALUES ( 'Eggplant');
INSERT INTO orders_topping ( name ) VALUES ( 'Tomato&Basil');
INSERT INTO orders_topping ( name ) VALUES ( 'GreenPeppers');
INSERT INTO orders_topping ( name ) VALUES ( 'Hamburger');
INSERT INTO orders_topping ( name ) VALUES ( 'Spinach');
INSERT INTO orders_topping ( name ) VALUES ( 'Artichoke');
INSERT INTO orders_topping ( name ) VALUES ( 'BuffaloChicken');
INSERT INTO orders_topping ( name ) VALUES ( 'BBQChicken');
INSERT INTO orders_topping ( name ) VALUES ( 'Anchovies');
INSERT INTO orders_topping ( name ) VALUES ( 'BlackOlives');
INSERT INTO orders_topping ( name ) VALUES ( 'FreshGarlic');
INSERT INTO orders_topping ( name ) VALUES ( 'Zucchini');
