Overview

In this project, an web application for handling a pizza restaurant’s online orders is built. Users will be able to browse the restaurant’s menu, add items to their cart, and submit their orders. Meanwhile, the restaurant owners will be able to add and update menu items, and view orders that have been placed.

Steps

Complete the Menu, Adding Items, and Registration/Login/Logout steps.
Complete the Shopping Cart and Placing an Order steps.
Complete the Viewing Orders and Personal Touch steps.

Run pip3 install -r requirements.txt 
Run python manage.py runserver to start up Django application.

Menu: The web application should support all of the available menu items for Pinnochio’s Pizza & Subs (a popular pizza place in Cambridge). Based on analyzing the menu and the various types of possible ordered items (small vs. large, toppings, additions, etc.) decide how to construct models to best represent the information. Add models to orders/models.py, make the necessary migration files, and apply those migrations.

Adding Items: Using Django Admin, site administrators (restaurant owners) should be able to add, update, and remove items on the menu. Add all of the items from the Pinnochio’s menu into your database using either the Admin UI or by running Python commands in Django’s shell.

Registration, Login, Logout: Site users (customers) should be able to register for the web application with a username, password, first name, last name, and email address. Customers should then be able to log in and log out of your website. Use Django’s built-in users and authentication system to simplify the process of logging users in and out.

Shopping Cart: Once logged in, users should see a representation of the restaurant’s menu, where they can add items (along with toppings or extras, if appropriate) to their virtual “shopping cart.” The contents of the shopping should be saved even if a user closes the window, or logs out and logs back in again.

Placing an Order: Once there is at least one item in a user’s shopping cart, they should be able to place an order, whereby the user is asked to confirm the items in the shopping cart, and the total (no need to worry about tax) before placing an order.

Viewing Orders: Site administrators should have access to a page where they can view any orders that have already been placed.

Personal Touch: Add at least one additional feature of your choosing to the web application. Possibilities include: allowing site administrators to mark orders as complete and allowing users to see the status of their pending or completed orders, integrating with the Stripe API to allow users to actually use a credit card to make a purchase during checkout, or supporting sending users a confirmation email once their purchase is complete. If need to use any credentials (like passwords or API credentials), be sure not to store any credentials in the source code, better to use environment variables.
